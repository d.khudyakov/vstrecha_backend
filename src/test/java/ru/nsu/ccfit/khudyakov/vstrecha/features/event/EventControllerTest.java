package ru.nsu.ccfit.khudyakov.vstrecha.features.event;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.nsu.ccfit.khudyakov.vstrecha.config.MapperConfig;
import ru.nsu.ccfit.khudyakov.vstrecha.config.authentication.TokenAuthenticationProvider;
import ru.nsu.ccfit.khudyakov.vstrecha.error.ApiError;
import ru.nsu.ccfit.khudyakov.vstrecha.error.GeneralErrorHandler;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.EventDateTimeDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.NewEventDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Importance;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = {EventController.class, MapperConfig.class, EventMapper.class, GeneralErrorHandler.class})
@AutoConfigureMockMvc(addFilters = false)
class EventControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TokenAuthenticationProvider tokenAuthenticationProvider;

    @MockBean
    private EventService eventService;

    private final EventController eventController;

    @Autowired
    EventControllerTest(EventController eventController) {
        this.eventController = eventController;
    }

    @Test
    @WithMockUser
    void createEvent_ExpectBadRequest_ValidationFailWithTitleAndDescription() throws Exception {
        NewEventDto eventWithDetailsDTO = new NewEventDto();

        EventDateTimeDto eventDateTimeDTO = new EventDateTimeDto();
        eventDateTimeDTO.setDateTime(LocalDateTime.parse("2011-06-03T10:00:00"));
        eventDateTimeDTO.setTimeZone(ZoneId.of("America/Los_Angeles"));

        eventWithDetailsDTO.setStart(eventDateTimeDTO);
        eventWithDetailsDTO.setEnd(eventDateTimeDTO);
        eventWithDetailsDTO.setImportance(Importance.HIGH);

        ApiError expectedApiError = new ApiError(1001,
                "Not all properties are filled in correctly",
                Sets.newTreeSet("Title cannot be empty"),
                null);

        MvcResult mvcResult = mockMvc
                .perform(post("/events")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(objectMapper.writeValueAsString(eventWithDetailsDTO))
                )
                .andExpect(status().isBadRequest())
                .andReturn();

        ApiError apiError = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ApiError.class);

        assertThat(apiError).isEqualTo(expectedApiError);
    }

}