create table meeting
(
    id         uuid        not null,
    title      varchar(40) not null,
    start_date timestamp   not null,
    end_date   timestamp   not null,
    time_zone  varchar(32) not null,
    invite_key varchar(6)  not null,
    creator_id int8        not null,
    primary key (id)
);

create table meeting_participants
(
    meetings_id     uuid not null,
    participants_id int8 not null
);

alter table meeting
    add constraint creator_id_fk foreign key (creator_id) references usr;

alter table meeting
    add constraint invite_key_uk unique (invite_key);

alter table meeting_participants
    add constraint participants_id_fk foreign key (participants_id) references usr;

alter table meeting_participants
    add constraint meetings_id_fk foreign key (meetings_id) references meeting;