create table usr
(
    id         bigserial   not null,
    email      varchar(32) not null,
    password   bpchar(60)  not null,
    token      uuid,
    first_name varchar(32) not null,
    last_name  varchar(32) not null,
    primary key (id)
);

alter table usr
    add constraint UK_email unique (email)