create table event
(
  id              uuid not null,
  description     varchar(200),
  title           varchar(40),
  end_date_time   timestamp,
  end_time_zone   varchar(32),
  start_date_time timestamp,
  start_time_zone varchar(32),
  recurrence_rule varchar(128),
  importance      int,
  user_id         bigint,
  primary key (id)
);

alter table event add constraint user_id_fk foreign key (user_id) references usr (id)
