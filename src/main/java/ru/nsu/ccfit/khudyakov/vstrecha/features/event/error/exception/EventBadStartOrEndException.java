package ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception;

public class EventBadStartOrEndException extends RuntimeException {

    public EventBadStartOrEndException(String message) {
        super(message);
    }

}
