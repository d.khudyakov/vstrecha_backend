package ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.deserializer.CustomJsonDateDeserializer;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.deserializer.CustomJsonZonIdDeserializer;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZoneId;

@ApiModel
@Data
public class EventDateTimeDto {

    @ApiModelProperty(value = "Describes the date and time in format ISO 8601")
    @NotNull(message = "Date time cannot be empty")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private LocalDateTime dateTime;

    @ApiModelProperty(value = "Describes time zone in tz database format", position = 1)
    @NotNull(message = "Time zone cannot be empty")
    @JsonDeserialize(using = CustomJsonZonIdDeserializer.class)
    private ZoneId timeZone;

}
