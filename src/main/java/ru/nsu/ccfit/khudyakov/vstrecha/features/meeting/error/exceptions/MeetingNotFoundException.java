package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.error.exceptions;

public class MeetingNotFoundException extends RuntimeException {
    private static final String ERROR_MSG = "Meeting not found";

    public MeetingNotFoundException() {
        super(ERROR_MSG);
    }

}
