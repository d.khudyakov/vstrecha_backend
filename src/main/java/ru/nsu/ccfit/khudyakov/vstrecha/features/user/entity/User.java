package ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Event;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.entity.Meeting;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "usr")
@Data
@EqualsAndHashCode(of = {"email"})
@ToString(exclude = "events")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Email
    @NotBlank
    @Column(unique = true)
    @Size(max = 32)
    private String email;

    @NotBlank
    @Column(columnDefinition = "bpchar(60)")
    private String password;

    @NotBlank
    @Size(max = 32)
    @Pattern(regexp = "^(?=.*[A-Za-z\\p{IsCyrillic}])[A-Za-z\\d\\p{IsCyrillic}]*$")
    private String firstName;

    @NotBlank(message = "Last name cannot be empty")
    @Size(max = 32)
    @Pattern(regexp = "^(?=.*[A-Za-z\\p{IsCyrillic}])[A-Za-z\\d\\p{IsCyrillic}]*$")
    private String lastName;

    private UUID token;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Event> events;

    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
    private List<Meeting> createdMeetings;

    @ManyToMany(mappedBy = "participants", cascade = CascadeType.REFRESH)
    private List<Meeting> meetings;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
