package ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception;

import java.text.MessageFormat;

public class EventBadZoneIdException extends RuntimeException {

    private static final String ERROR_MSG = "Bad time zone: {0}";

    public EventBadZoneIdException(String date) {
        super(MessageFormat.format(ERROR_MSG, date));
    }

}
