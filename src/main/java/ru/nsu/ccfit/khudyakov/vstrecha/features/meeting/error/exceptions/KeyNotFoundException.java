package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.error.exceptions;

public class KeyNotFoundException extends RuntimeException {
    private static final String ERROR_MSG = "No meeting with the given key was found";

    public KeyNotFoundException() {
        super(ERROR_MSG);
    }
}
