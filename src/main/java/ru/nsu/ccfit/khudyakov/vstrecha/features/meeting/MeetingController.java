package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.khudyakov.vstrecha.error.ApiError;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.dto.MeetingInfoDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.dto.MeetingSummaryDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.dto.NewMeetingDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.entity.Meeting;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;
import ru.nsu.ccfit.khudyakov.vstrecha.general.model.EmptyJsonResponseDto;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Api(value = "Controller for meeting organization")
@RequestMapping("/meetings")
@RestController
@RequiredArgsConstructor
public class MeetingController {

    private final MeetingService meetingService;

    private final ModelMapper modelMapper;

    @PostMapping
    @ApiOperation(value = "Create meeting", response = EmptyJsonResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Meeting created"),
            @ApiResponse(code = 400, message = "Meeting creation error", response = ApiError.class),
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createMeeting(
            @ApiIgnore @AuthenticationPrincipal User user,
            @Valid @RequestBody NewMeetingDto newMeetingDto) {
        meetingService.createMeeting(user, modelMapper.map(newMeetingDto, Meeting.class));
    }

    @GetMapping
    @ApiOperation(value = "Get meetings summaries", response = EmptyJsonResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Error", response = ApiError.class),
    })
    public List<MeetingSummaryDto> getMeetings(@ApiIgnore @AuthenticationPrincipal User user) {

        return meetingService.getMeetings(user).stream()
                .map(m -> modelMapper.map(m, MeetingSummaryDto.class))
                .collect(toList());
    }

    @GetMapping("/{meetingId}")
    @ApiOperation(value = "Get meeting", response = EmptyJsonResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Failed to get meeting", response = ApiError.class),
            @ApiResponse(code = 404, message = "Meeting not found", response = ApiError.class)
    })
    public MeetingInfoDto getMeeting(
            @ApiIgnore @AuthenticationPrincipal User user,
            @PathVariable("meetingId") @NotNull(message = "Meeting id cannot be empty") UUID id) {
        return modelMapper.map(meetingService.getMeetingsInfo(user, id), MeetingInfoDto.class);

    }

    @PostMapping("/{meetingId}/actions/leaving")
    @ApiOperation(value = "Leave meeting", response = EmptyJsonResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success"),
            @ApiResponse(code = 400, message = "Failed to leave meeting", response = ApiError.class),
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void leaveMeeting(@ApiIgnore @AuthenticationPrincipal User user,
                             @PathVariable("meetingId") @NotNull(message = "Meeting id cannot be empty") UUID id) {
        meetingService.leaveMeeting(user, id);
    }

    @PostMapping("/{inviteKey}/actions/joining")
    @ApiOperation(value = "Join meeting", response = EmptyJsonResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success"),
            @ApiResponse(code = 400, message = "Failed to join meeting", response = ApiError.class),
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void joinMeeting(@ApiIgnore @AuthenticationPrincipal User user,
                            @PathVariable("inviteKey") @NotBlank(message = "Meeting id cannot be empty") String inviteKey) {
        meetingService.joinMeeting(user, inviteKey);
    }

}
