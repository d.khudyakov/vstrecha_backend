package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel
public class Participant {

    @ApiModelProperty(value = "user's first name")
    private String firstName;

    @ApiModelProperty(value = "user's last name")
    private String lastName;

}
