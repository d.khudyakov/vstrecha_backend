package ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity;

import lombok.Data;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Data
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private User user;

    @NotBlank
    @Size(max = 40)
    private String title;

    @Size(max = 200)
    private String description;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "dateTime", column = @Column(name = "start_date_time")),
            @AttributeOverride(name = "timeZone", column = @Column(name = "start_time_zone")),
    })
    private EventDateTime start;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "dateTime", column = @Column(name = "end_date_time")),
            @AttributeOverride(name = "timeZone", column = @Column(name = "end_time_zone")),
    })
    private EventDateTime end;

    @Enumerated(EnumType.ORDINAL)
    private Importance importance;

    private String recurrenceRule;

    @Transactional
    public void setUser(User user) {
        this.user = user;
    }

}
