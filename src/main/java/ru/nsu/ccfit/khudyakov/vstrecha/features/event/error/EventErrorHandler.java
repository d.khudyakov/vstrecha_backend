package ru.nsu.ccfit.khudyakov.vstrecha.features.event.error;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.nsu.ccfit.khudyakov.vstrecha.error.ApiError;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception.*;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.Collections;

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class EventErrorHandler {

    private final static int BAD_EVENT_DATE_TIME_ERROR_CODE = 3001;
    private final static int BAD_ZONE_ID_TIME_ERROR_CODE = 3002;
    private final static int BAD_RECURRENCE_RULE_ERROR_CODE = 3003;
    private final static int BAD_START_OR_END_ERROR_CODE = 3004;
    private final static int NOT_CORRECT_EVENT_ERROR_CODE = 3005;
    private final static int NOT_CORRECT_RRULE_CODE = 3005;

    @ExceptionHandler(value = {EventBadDateTimeException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleDateTimeParseError(EventBadDateTimeException ex) {
        return new ApiError(
                BAD_EVENT_DATE_TIME_ERROR_CODE,
                ex.getMessage(),
                Collections.singleton("Date and time must be in format ISO 8601: yyyy-MM-ddTHH:mm:ss"),
                LocalDateTime.now());
    }

    @ExceptionHandler(value = {EventBadZoneIdException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleTimeZoneParseError(EventBadZoneIdException ex) {
        return new ApiError(
                BAD_ZONE_ID_TIME_ERROR_CODE,
                ex.getMessage(),
                Collections.singleton("Time zone must be from tz database"),
                LocalDateTime.now());
    }

    @ExceptionHandler(value = {EventBadRecurrenceRuleException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleRecurrenceRuleError(EventBadRecurrenceRuleException ex) {
        return new ApiError(
                BAD_RECURRENCE_RULE_ERROR_CODE,
                ex.getMessage(),
                Collections.singleton("Recurrence rule must be in RFC5545 format"),
                LocalDateTime.now());
    }

    @ExceptionHandler(value = {EventBadStartOrEndException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleStartOrEndError(EventBadStartOrEndException ex) {
        return new ApiError(
                BAD_START_OR_END_ERROR_CODE,
                ex.getMessage(),
                Collections.singleton("Please check your start dates and times"),
                LocalDateTime.now());
    }

    @ExceptionHandler(value = {DateTimeException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleStartOrEndError(DateTimeException ex) {
        return new ApiError(
                BAD_START_OR_END_ERROR_CODE,
                ex.getMessage(),
                Collections.singleton("Please check your start dates and times"),
                LocalDateTime.now());
    }

    @ExceptionHandler(value = {NotCorrectEventException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleNotCorrectEvent(NotCorrectEventException ex) {
        return new ApiError(
                NOT_CORRECT_EVENT_ERROR_CODE,
                ex.getMessage(),
                Collections.singleton("Please check id of your event"),
                LocalDateTime.now());
    }

}
