package ru.nsu.ccfit.khudyakov.vstrecha.features.user.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.nsu.ccfit.khudyakov.vstrecha.error.ApiError;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.error.exception.AlreadyExistsUserException;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.error.exception.BadPasswordOrEmailException;

import java.time.LocalDateTime;
import java.util.Collections;

@RestControllerAdvice
public class UserErrorHandler {
    private final static int BAD_EMAIL_OR_PASSWORD_ERROR_CODE = 2001;
    private final static int ALREADY_EXISTS_USER_ERROR_CODE = 2002;

    @ExceptionHandler(value = {BadPasswordOrEmailException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleBadPasswordOrEmailException(BadPasswordOrEmailException ex) {
        return new ApiError(
                BAD_EMAIL_OR_PASSWORD_ERROR_CODE,
                ex.getMessage(),
                Collections.singleton("Please check your email and password"),
                LocalDateTime.now());
    }

    @ExceptionHandler(value = {AlreadyExistsUserException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleAlreadyExistsUserException(AlreadyExistsUserException ex) {
        return new ApiError(
                ALREADY_EXISTS_USER_ERROR_CODE,
                ex.getMessage(),
                Collections.singleton("Please try another email"),
                LocalDateTime.now());
    }


}
