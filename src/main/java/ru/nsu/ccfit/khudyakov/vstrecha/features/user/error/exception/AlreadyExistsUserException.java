package ru.nsu.ccfit.khudyakov.vstrecha.features.user.error.exception;

import java.text.MessageFormat;

public class AlreadyExistsUserException extends RuntimeException {
    private static final String ERROR_MSG = "User with email: {0} already exists";

    public AlreadyExistsUserException(String email) {
        super(MessageFormat.format(ERROR_MSG, email));
    }
}
