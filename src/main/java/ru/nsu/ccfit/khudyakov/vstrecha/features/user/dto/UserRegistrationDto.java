package ru.nsu.ccfit.khudyakov.vstrecha.features.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
public class UserRegistrationDto extends UserLoginDto {

    @ApiModelProperty(value = "user's first name")
    @NotBlank(message = "First name cannot be empty")
    @Size(max = 32)
    @Pattern(regexp = "^(?=.*[A-Za-z\\p{IsCyrillic}])[A-Za-z\\d\\p{IsCyrillic}]*$",
            message = "First name does not meet requirements: must contain at least one letter of the Russian / English alphabet")
    private String firstName;

    @ApiModelProperty(value = "user's last name", position = 1)
    @NotBlank(message = "Last name cannot be empty")
    @Size(max = 32)
    @Pattern(regexp = "^(?=.*[A-Za-z\\p{IsCyrillic}])[A-Za-z\\d\\p{IsCyrillic}]*$",
            message = "Last name does not meet requirements: must contain at least one letter of the Russian / English alphabet")
    private String lastName;
}
