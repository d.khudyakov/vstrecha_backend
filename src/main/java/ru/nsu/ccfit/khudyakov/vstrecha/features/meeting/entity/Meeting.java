package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.UUID;

@Data
@EqualsAndHashCode(of = "inviteKey")
@Entity
@ToString(exclude = {"participants"})
public class Meeting {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NotBlank
    @Size(max = 40)
    private String title;

    @NotNull
    @Convert(converter = Jsr310JpaConverters.LocalDateConverter.class)
    @Column(name = "start_date")
    private LocalDate start;

    @NotNull
    @Convert(converter = Jsr310JpaConverters.LocalDateConverter.class)
    @Column(name = "end_date")
    protected LocalDate end;

    @NotNull
    @Convert(converter = Jsr310JpaConverters.ZoneIdConverter.class)
    private ZoneId timeZone;

    @NotBlank
    @Column(unique = true)
    private String inviteKey;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private User creator;

    @ManyToMany
    private List<User> participants;

}
