package ru.nsu.ccfit.khudyakov.vstrecha.features.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    User findByToken(UUID token);
}
