package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
@ApiModel
public class MeetingSummaryDto {

    @ApiModelProperty(value = "id")
    private UUID id;

    @ApiModelProperty(value = "Title")
    private String title;

    @ApiModelProperty(value = "Start date of the selection period")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate start;

    @ApiModelProperty(value = "End date of the selection period")
    @JsonFormat(pattern = "yyyy-MM-dd")
    protected LocalDate end;

}
