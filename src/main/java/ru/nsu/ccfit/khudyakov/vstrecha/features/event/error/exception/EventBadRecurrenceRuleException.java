package ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception;

import java.text.MessageFormat;

public class EventBadRecurrenceRuleException extends RuntimeException {
    private static final String ERROR_MSG = "Invalid recurrence rule: {0}";

    public EventBadRecurrenceRuleException(String message) {
        super(MessageFormat.format(ERROR_MSG, message));
    }
}
