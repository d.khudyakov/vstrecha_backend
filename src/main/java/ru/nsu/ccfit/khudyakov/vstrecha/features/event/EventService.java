package ru.nsu.ccfit.khudyakov.vstrecha.features.event;

import lombok.RequiredArgsConstructor;
import org.dmfs.rfc5545.recur.RecurrenceRuleIterator;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Event;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.EventDateTime;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception.NotCorrectEventException;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.model.EventSummary;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.time.Duration.between;
import static java.util.stream.Collectors.toList;
import static ru.nsu.ccfit.khudyakov.vstrecha.utils.EventUtils.convertToLocalDateTime;
import static ru.nsu.ccfit.khudyakov.vstrecha.utils.EventUtils.getRecurrenceRuleIterator;

@Service
@RequiredArgsConstructor
public class EventService {

    private final EventRepository eventRepository;

    private final ModelMapper modelMapper;

    public void createEvent(User user, Event event) {
        user.getEvents().add(event);
        event.setUser(user);
        eventRepository.save(event);
    }

    public void updateEvent(User user, Event event) {
        event.setUser(user);

        if (eventRepository.findById(event.getId()).isPresent() || user.getEvents().contains(event)) {
            eventRepository.save(event);
        } else {
            throw new NotCorrectEventException("Not correct event for updating");
        }
    }

    public void deleteEvent(User user, UUID uuid) {
        Optional<Event> event = eventRepository.findById(uuid);
        Event innerEvent = event.orElseThrow(() -> new NotCorrectEventException("Not correct id of deleting ever"));

        user.getEvents().remove(innerEvent);
        innerEvent.setUser(null);

        eventRepository.delete(innerEvent);
    }


    public List<EventSummary> getEventsForPeriod(User user, LocalDateTime start, LocalDateTime end) {
        List<Event> singleEvents = eventRepository.findByUserAndStartDateTimeBetweenAndRecurrenceRuleIsNull(user, start, end);
        List<Event> recurrenceEvents = eventRepository.findByUserAndRecurrenceRuleNotNull(user);

        List<EventSummary> eventSummaryList = singleEvents.stream()
                .map(e -> modelMapper.map(e, EventSummary.class))
                .collect(toList());

        eventSummaryList.addAll(findByRecurrenceRule(start, end, recurrenceEvents));

        return eventSummaryList;
    }

    private List<EventSummary> findByRecurrenceRule(LocalDateTime start, LocalDateTime end, List<Event> recurrenceEvents) {
        List<EventSummary> eventSummaryList = new ArrayList<>();

        for (Event event : recurrenceEvents) {
            Duration duration = between(event.getStart().getDateTime(), event.getEnd().getDateTime());

            RecurrenceRuleIterator startIterator = getRecurrenceRuleIterator(
                    event.getRecurrenceRule(),
                    event.getStart().getDateTime(),
                    start);

            if (startIterator != null) {
                while (startIterator.hasNext()) {
                    LocalDateTime instanceStartDateTime = convertToLocalDateTime(startIterator.nextDateTime());

                    if (instanceStartDateTime.isAfter(end)) {
                        break;
                    } else {
                        eventSummaryList.add(getEventSummary(event, instanceStartDateTime, duration));
                    }
                }
            }
        }

        return eventSummaryList;
    }

    private EventSummary getEventSummary(Event event, LocalDateTime start, Duration duration) {
        EventSummary eventSummary = new EventSummary();

        eventSummary.setId(event.getId());
        eventSummary.setTitle(event.getTitle());

        EventDateTime startEventEventDateTime = new EventDateTime();
        startEventEventDateTime.setDateTime(start);
        startEventEventDateTime.setTimeZone(event.getStart().getTimeZone());
        eventSummary.setStart(startEventEventDateTime);

        EventDateTime endEventDateTime = new EventDateTime();
        endEventDateTime.setDateTime(start.plus(duration));
        endEventDateTime.setTimeZone(event.getEnd().getTimeZone());
        eventSummary.setEnd(endEventDateTime);

        return eventSummary;
    }


    public Event getEvent(User user, UUID eventId) {
        Event event = eventRepository.findById(eventId).orElseThrow(
                () -> new NotCorrectEventException(MessageFormat.format("Event with id {0} doesn't exist", eventId)));
        if (user.getEvents().contains(event)) {
            return event;
        } else {
            throw new NotCorrectEventException("Not enough rights to receive this event");
        }
    }

}
