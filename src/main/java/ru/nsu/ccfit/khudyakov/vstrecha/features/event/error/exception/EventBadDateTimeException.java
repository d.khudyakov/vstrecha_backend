package ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception;

import java.text.MessageFormat;

public class EventBadDateTimeException extends RuntimeException {
    private static final String ERROR_MSG = "Bad date time: {0}";

    public EventBadDateTimeException(String date) {
        super(MessageFormat.format(ERROR_MSG, date));
    }
}
