package ru.nsu.ccfit.khudyakov.vstrecha.features.event;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.dmfs.rfc5545.recur.InvalidRecurrenceRuleException;
import org.dmfs.rfc5545.recur.RecurrenceRule;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.khudyakov.vstrecha.error.ApiError;
import ru.nsu.ccfit.khudyakov.vstrecha.error.exception.BadParamsException;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.EventDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.EventSummaryDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.NewEventDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Event;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception.EventBadRecurrenceRuleException;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception.EventBadStartOrEndException;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.model.EventSummary;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;
import ru.nsu.ccfit.khudyakov.vstrecha.general.model.EmptyJsonResponseDto;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static java.time.LocalTime.MAX;
import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/events")
@Api(value = "Controller for events")
@RequiredArgsConstructor
public class EventController {

    private final EventMapper eventMapper;

    private final EventService eventService;

    @PostMapping
    @ApiOperation(value = "Create event in personal calendar", response = EmptyJsonResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Event created"),
            @ApiResponse(code = 400, message = "Event creation error", response = ApiError.class),
    })
    @ResponseStatus(HttpStatus.CREATED)
    public EmptyJsonResponseDto createEvent(
            @ApiIgnore @AuthenticationPrincipal User user,
            @Valid @RequestBody NewEventDto newEventDTO) {

        if (newEventDTO.getRecurrenceRule() != null) {
            validateRecurrenceRule(newEventDTO.getRecurrenceRule());
        }

        validateDateTimes(newEventDTO);

        Event event = eventMapper.toEventEntity(newEventDTO);
        eventService.createEvent(user, event);

        return new EmptyJsonResponseDto();
    }

    private void validateDateTimes(NewEventDto newEventDto) {
        if (!newEventDto.getStart().getDateTime().isBefore(newEventDto.getEnd().getDateTime())) {
            throw new EventBadStartOrEndException("Start is before end");
        }

        LocalDate startLocalDate = newEventDto.getStart().getDateTime().toLocalDate();
        LocalDate endLocalDate = newEventDto.getEnd().getDateTime().toLocalDate();

        if (!startLocalDate.isEqual(endLocalDate)) {
            throw new EventBadStartOrEndException("Start and end date do not match");
        }

        if (!newEventDto.getStart().getTimeZone().equals(newEventDto.getEnd().getTimeZone())) {
            throw new EventBadStartOrEndException("Start and end timezones do not match");
        }

    }

    private void validateRecurrenceRule(String recurrenceRuleString) {
        try {
            new RecurrenceRule(recurrenceRuleString);
        } catch (InvalidRecurrenceRuleException e) {
            throw new EventBadRecurrenceRuleException(recurrenceRuleString);
        }
    }

    @PutMapping
    @ApiOperation(value = "Update all fields of event")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = EmptyJsonResponseDto.class),
            @ApiResponse(code = 400, message = "Not correct event for updating", response = ApiError.class),
    })
    public EmptyJsonResponseDto updateEvent(
            @ApiIgnore @AuthenticationPrincipal User user,
            @Valid @RequestBody EventDto eventDTO) {

        eventService.updateEvent(user, eventMapper.toEventEntity(eventDTO));
        return new EmptyJsonResponseDto();
    }

    @DeleteMapping("/{eventId}")
    @ApiOperation(value = "Delete event")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success"),
            @ApiResponse(code = 400, message = "Not correct event for deleting", response = ApiError.class),
    })
    public void deleteEvent(
            @ApiIgnore @AuthenticationPrincipal User user,
            @PathVariable("eventId") @NotNull(message = "Event id cannot be empty") UUID id) {

        eventService.deleteEvent(user, id);
    }

    @GetMapping("/{eventId}")
    @ApiOperation(value = "Get full information about event")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = EventDto.class),
            @ApiResponse(code = 400, message = "Cannot get event", response = ApiError.class),
    })
    public EventDto getEvent(
            @ApiIgnore @AuthenticationPrincipal User user,
            @PathVariable("eventId") @NotNull(message = "Event id cannot be empty") UUID id) {
        return eventMapper.toEventDTO(eventService.getEvent(user, id));
    }

    @GetMapping("/day/{year}/{month}/{day}")
    @ApiOperation(value = "Get events for specific day", response = EventSummaryDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Not correct query parameters", response = ApiError.class),
    })
    public List<EventSummaryDto> getEventsForSpecificDay(
            @ApiIgnore @AuthenticationPrincipal User user,
            @PathVariable Integer year, @PathVariable Integer month, @PathVariable Integer day) {

        try {
            LocalDate localDate = LocalDate.of(year, month, day);
            List<EventSummary> eventList = eventService.getEventsForPeriod(user, localDate.atStartOfDay(), localDate.atTime(MAX));

            return convertEventSummaries(eventList);
        } catch (DateTimeException ex) {
            throw new BadParamsException(ex.getMessage());
        }

    }

    @GetMapping("week/{year}/{month}/{day}")
    @ApiOperation(value = "Get events for specific week witch starting from the day")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = EventSummaryDto.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Not correct query parameters", response = ApiError.class),
    })
    public List<EventSummaryDto> getEventsForSpecificWeek(
            @ApiIgnore @AuthenticationPrincipal User user,
            @PathVariable Integer year, @PathVariable Integer month, @PathVariable Integer day) {

        try {
            LocalDate localDate = LocalDate.of(year, month, day);

            List<EventSummary> eventList = eventService.getEventsForPeriod(user,
                    localDate.atStartOfDay(),
                    localDate.plusWeeks(1).atTime(MAX).minusDays(1));

            return convertEventSummaries(eventList);
        } catch (DateTimeException ex) {
            throw new BadParamsException(ex.getMessage());
        }

    }

    private List<EventSummaryDto> convertEventSummaries(List<EventSummary> eventList) {
        return eventList.stream()
                .map(eventMapper::toEventSummaryWithIdDTO)
                .collect(toList());
    }

}
