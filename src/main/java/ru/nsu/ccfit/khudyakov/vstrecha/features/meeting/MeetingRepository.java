package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.entity.Meeting;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MeetingRepository extends JpaRepository<Meeting, UUID> {

    Optional<Meeting> findByInviteKey(String inviteKey);

}
