package ru.nsu.ccfit.khudyakov.vstrecha.features.user;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.ccfit.khudyakov.vstrecha.config.authentication.TokenAuth;
import ru.nsu.ccfit.khudyakov.vstrecha.error.ApiError;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.dto.UserLoginDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.dto.UserRegistrationDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.error.exception.BadPasswordOrEmailException;
import ru.nsu.ccfit.khudyakov.vstrecha.general.model.EmptyJsonResponseDto;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@RestController
@Api(value = "Controller for user login and registration")
public class UserLoginController {

    private final UserAuthService authenticationService;

    private final ModelMapper modelMapper;

    @Autowired
    public UserLoginController(UserAuthService authenticationService, ModelMapper modelMapper) {
        this.authenticationService = authenticationService;
        this.modelMapper = modelMapper;
    }

    @ApiOperation(value = "User login", response = TokenAuth.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sign in correctly"),
            @ApiResponse(code = 400, message = "Login error", response = ApiError.class),
    })
    @PostMapping(value = "/sign_in")
    public TokenAuth login(@Valid @RequestBody UserLoginDto userLoginDTO) {
        return authenticationService.signIn(userLoginDTO.getEmail(), userLoginDTO.getPassword())
                .orElseThrow(BadPasswordOrEmailException::new);
    }

    @ApiOperation(value = "User registration", response = EmptyJsonResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "User was created"),
            @ApiResponse(code = 400, message = "Registration error", response = ApiError.class),
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/sign_up")
    public EmptyJsonResponseDto registerUser(@Valid @RequestBody UserRegistrationDto userRegistrationDTO) {
        authenticationService.signUp(modelMapper.map(userRegistrationDTO, User.class));

        return new EmptyJsonResponseDto();
    }

    @ApiOperation(value = "User logout", response = EmptyJsonResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful logout"),
            @ApiResponse(code = 401, message = "Unauthorized -  not correct token"),
    })
    @PostMapping(value = "/logout")
    public EmptyJsonResponseDto logout(@ApiIgnore @AuthenticationPrincipal final User user) {
        authenticationService.logout(user);

        return new EmptyJsonResponseDto();
    }

}
