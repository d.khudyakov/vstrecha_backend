package ru.nsu.ccfit.khudyakov.vstrecha.features.event;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.EventDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.EventSummaryDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.NewEventDto;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Event;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.model.EventSummary;

@Component
@RequiredArgsConstructor
public class EventMapper {

    private final ModelMapper mapper;

    Event toEventEntity(NewEventDto dto) {
        return mapper.map(dto, Event.class);
    }

    Event toEventEntity(EventDto dto) {
        return mapper.map(dto, Event.class);
    }

    EventSummaryDto toEventSummaryWithIdDTO(EventSummary eventSummary) {
        return mapper.map(eventSummary, EventSummaryDto.class);
    }

    EventDto toEventDTO(Event event) {
        return mapper.map(event, EventDto.class);
    }

}