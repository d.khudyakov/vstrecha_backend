package ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity;

public enum Importance {
    LOW,
    MEDIUM,
    HIGH
}
