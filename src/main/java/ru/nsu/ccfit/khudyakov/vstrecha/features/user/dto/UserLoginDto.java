package ru.nsu.ccfit.khudyakov.vstrecha.features.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@ApiModel
public class UserLoginDto {

    @ApiModelProperty(value = "user's email")
    @Email(message = "Email is not correct")
    @NotBlank(message = "Email cannot be empty")
    private String email;

    @ApiModelProperty(value = "user's password", position = 1)
    @NotBlank(message = "Password cannot be empty")
    @Size(max = 32)
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
            message = "Password does not meet requirements: minimum eight characters, at least one letter and one number")
    private String password;

}
