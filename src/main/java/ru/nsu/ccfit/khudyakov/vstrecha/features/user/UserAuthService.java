package ru.nsu.ccfit.khudyakov.vstrecha.features.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.ccfit.khudyakov.vstrecha.config.authentication.TokenAuth;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.error.exception.AlreadyExistsUserException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserAuthService {

    @PersistenceContext
    private EntityManager entityManager;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Transactional
    public void signUp(User user) {
        if (userRepository.findByEmail(user.getEmail()) != null) {
            throw new AlreadyExistsUserException(user.getEmail());
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        entityManager.persist(user);
    }

    public Optional<TokenAuth> signIn(String email, String password) {
        TokenAuth tokenAuth = null;
        User user = userRepository.findByEmail(email);

        if (user != null && passwordEncoder.matches(password, user.getPassword())) {
            UUID token = UUID.randomUUID();
            user.setToken(token);
            userRepository.save(user);
            tokenAuth = new TokenAuth(token.toString());
        }

        return Optional.ofNullable(tokenAuth);
    }

    public Optional<User> findByToken(String token) {
        return Optional.ofNullable(userRepository.findByToken(UUID.fromString(token)));
    }

    public void logout(User user) {
        user.setToken(null);
        userRepository.save(user);
    }

}
