package ru.nsu.ccfit.khudyakov.vstrecha.features.event.model;

import lombok.Data;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.EventDateTime;

import java.util.UUID;

@Data
public class EventSummary {

    private UUID id;

    private String title;

    private EventDateTime start;

    private EventDateTime end;

}
