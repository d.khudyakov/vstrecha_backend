package ru.nsu.ccfit.khudyakov.vstrecha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VstrechaApplication {

    public static void main(String[] args) {
        SpringApplication.run(VstrechaApplication.class, args);
    }


}
