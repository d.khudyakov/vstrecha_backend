package ru.nsu.ccfit.khudyakov.vstrecha.utils;

import org.dmfs.rfc5545.DateTime;
import org.dmfs.rfc5545.recur.InvalidRecurrenceRuleException;
import org.dmfs.rfc5545.recur.RecurrenceRule;
import org.dmfs.rfc5545.recur.RecurrenceRuleIterator;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.format.DateTimeFormatter.ofPattern;
import static org.dmfs.rfc5545.DateTime.parse;


@Component
public class EventUtils {


    private static final DateTimeFormatter dateTimeFormatter = ofPattern("yyyyMMdd'T'HHmmss");


    public static LocalDateTime convertToLocalDateTime(DateTime dateTime) {
        return LocalDateTime.parse(dateTime.toString(), dateTimeFormatter);
    }

    public static RecurrenceRuleIterator getRecurrenceRuleIterator(String rule,
                                                                   LocalDateTime start,
                                                                   LocalDateTime intervalStart) {
        try {
            RecurrenceRule recurrenceRule = new RecurrenceRule(rule);
            RecurrenceRuleIterator startIterator = recurrenceRule.iterator(parse(start.format(dateTimeFormatter)));
            startIterator.fastForward(parse(intervalStart.format(dateTimeFormatter)));
            return startIterator;

        } catch (InvalidRecurrenceRuleException e) {
            e.printStackTrace();
        }

        return null;
    }

}
