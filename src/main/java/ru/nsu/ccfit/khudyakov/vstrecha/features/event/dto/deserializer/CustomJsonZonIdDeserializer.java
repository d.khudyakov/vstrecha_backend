package ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception.EventBadZoneIdException;

import java.io.IOException;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.time.zone.ZoneRulesException;

public class CustomJsonZonIdDeserializer extends JsonDeserializer<ZoneId> {
    @Override
    public ZoneId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

        String timeZone = jsonParser.getText();
        try {
            return ZoneId.of(timeZone);
        } catch (DateTimeParseException | ZoneRulesException e) {
            throw new EventBadZoneIdException(timeZone);
        }
    }
}
