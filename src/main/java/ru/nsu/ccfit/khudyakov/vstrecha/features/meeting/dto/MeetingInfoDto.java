package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel
public class MeetingInfoDto extends MeetingSummaryDto {

    @ApiModelProperty(value = "Invite key")
    private String inviteKey;

    @ApiModelProperty(value = "Days with favorable factors")
    private List<DayDto> days;

    @ApiModelProperty(value = "Participants")
    private List<ParticipantDto> participants;

}
