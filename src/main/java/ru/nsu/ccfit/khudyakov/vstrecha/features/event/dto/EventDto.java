package ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Importance;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@ApiModel
@Data
public class EventDto {

    @NotNull(message = "Id cannot be null")
    private UUID id;

    @ApiModelProperty(value = "Event's title")
    protected String title;

    @ApiModelProperty(value = "Event's start date and time with timezone", position = 1)
    protected EventDateTimeDto start;

    @ApiModelProperty(value = "Event's end date and time with timezone", position = 2)
    protected EventDateTimeDto end;

    @ApiModelProperty(value = "Event's description")
    private String description;

    @ApiModelProperty(value = "Event's importance. Possible values: LOW, MEDIUM, HIGH", position = 1)
    private Importance importance;

    @ApiModelProperty(value = "Event's recurrence rule (RFC5545 RRULE). Optional", position = 2)
    private String recurrenceRule;

}
