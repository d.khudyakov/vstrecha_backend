package ru.nsu.ccfit.khudyakov.vstrecha.features.user.error.exception;

public class BadPasswordOrEmailException extends RuntimeException {
    private static final String ERROR_MSG = "Bad email or password";

    public BadPasswordOrEmailException() {
        super(ERROR_MSG);
    }
}
