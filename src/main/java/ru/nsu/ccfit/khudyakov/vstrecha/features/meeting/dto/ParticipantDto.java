package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class ParticipantDto {

    @ApiModelProperty(value = "user's first name")
    private String firstName;

    @ApiModelProperty(value = "user's last name")
    private String lastName;

}
