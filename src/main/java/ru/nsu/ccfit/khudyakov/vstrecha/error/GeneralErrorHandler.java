package ru.nsu.ccfit.khudyakov.vstrecha.error;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.nsu.ccfit.khudyakov.vstrecha.error.exception.BadParamsException;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
public class GeneralErrorHandler extends ResponseEntityExceptionHandler {
    private final static int NOT_VALID_FIELDS_ERROR = 1001;
    private final static int MISSING_PARAM_ERROR = 1002;
    private final static int BAD_PARAMS_ERROR = 1003;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        Set<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toSet());

        ApiError apiError = new ApiError(
                NOT_VALID_FIELDS_ERROR,
                "Not all properties are filled in correctly",
                errors,
                LocalDateTime.now()
        );

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        ApiError apiError = new ApiError(
                MISSING_PARAM_ERROR,
                ex.getLocalizedMessage(),
                Collections.singleton(ex.getParameterName()),
                LocalDateTime.now()
        );

        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {BadParamsException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleBadParams(BadParamsException ex) {
        return new ApiError(
                BAD_PARAMS_ERROR,
                ex.getMessage(),
                Collections.singleton("Please check parameters of request"),
                LocalDateTime.now());
    }
}
