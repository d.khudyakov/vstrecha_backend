package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.model.Busyness;

@Data
@ApiModel
public class DayDto {

    @ApiModelProperty(value = "Busyness of day")
    private Busyness busyness;

}
