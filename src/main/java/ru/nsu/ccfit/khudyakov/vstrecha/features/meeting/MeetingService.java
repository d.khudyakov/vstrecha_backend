package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting;

import lombok.RequiredArgsConstructor;
import org.dmfs.rfc5545.recur.RecurrenceRuleIterator;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Event;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.EventDateTime;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Importance;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.entity.Meeting;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.error.exceptions.KeyNotFoundException;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.error.exceptions.MeetingNotFoundException;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.model.*;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;

import javax.transaction.Transactional;
import java.time.*;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static ru.nsu.ccfit.khudyakov.vstrecha.utils.EventUtils.convertToLocalDateTime;
import static ru.nsu.ccfit.khudyakov.vstrecha.utils.EventUtils.getRecurrenceRuleIterator;

@Service
@RequiredArgsConstructor
public class MeetingService {

    private static final int KEY_LENGTH = 6;

    private final MeetingRepository meetingRepository;

    @Transactional
    public void createMeeting(User user, Meeting meeting) {
        generateInviteKey(meeting);

        user.getCreatedMeetings().add(meeting);
        user.getMeetings().add(meeting);

        meeting.setCreator(user);
        meeting.setParticipants(new ArrayList<>());
        meeting.getParticipants().add(user);

        meetingRepository.save(meeting);
    }
    
    private void generateInviteKey(Meeting meeting) {
        boolean success = false;
        do {
            String inviteKey = randomAlphanumeric(KEY_LENGTH);
            if (!meetingRepository.findByInviteKey(inviteKey).isPresent()) {
                meeting.setInviteKey(inviteKey);
                success = true;
            }

        } while (!success);
    }

    public List<Meeting> getMeetings(User user) {
        return user.getMeetings();
    }

    public MeetingInfo getMeetingsInfo(User user, UUID id) {
        Optional<Meeting> meetingOptional = meetingRepository.findById(id);
        Meeting meeting = meetingOptional.orElseThrow(MeetingNotFoundException::new);

        List<Participant> participants = new ArrayList<>();
        List<Day> days = new ArrayList<>();

        for (User participant : meeting.getParticipants()) {
            participants.add(new Participant(participant.getFirstName(), participant.getLastName()));
        }

        for (LocalDate date = meeting.getStart(); date.isBefore(meeting.getEnd().plusDays(1)); date = date.plusDays(1)) {
            int k = 0;
            for (User participant : meeting.getParticipants()) {
                if (getDayBusynessFactor(participant, date, meeting.getTimeZone()) < 0.75) {
                    continue;
                }
                ++k;
            }

            float q = (k * 100f) / meeting.getParticipants().size();
            days.add(getDay(q));
        }

        return new MeetingInfo(
                meeting.getId(),
                meeting.getTitle(),
                meeting.getStart(),
                meeting.getEnd(),
                meeting.getInviteKey(),
                days,
                participants
        );

    }

    private Day getDay(float q) {
        if (q > 75) {
            return new Day(Busyness.LOW);
        } else if (q > 55) {
            return new Day(Busyness.MEDIUM);
        } else {
            return new Day(Busyness.HIGH);
        }
    }

    public float getDayBusynessFactor(User participant, LocalDate date, ZoneId targetZone) {
        List<TransferredEvent> transferredEvents = transferEvents(participant.getEvents(), targetZone);

        Map<Importance, Duration> importanceDurationMap = new EnumMap<>(Importance.class);

        LocalDateTime dayStart = date.atStartOfDay();
        LocalDateTime dayEnd = date.atStartOfDay().plusDays(1).minusSeconds(1);

        for (TransferredEvent event : transferredEvents) {
            Importance importance = event.getImportance();

            if (event.getRecurrenceRule() == null) {
                updateImportanceDuration(importanceDurationMap, importance, dayStart, dayEnd, event.getStart(), event.getEnd());
                continue;
            }

            RecurrenceRuleIterator startIterator = getRecurrenceRuleIterator(
                    event.getRecurrenceRule(),
                    event.getStart(),
                    dayStart);
            Duration duration = Duration.between(event.getStart(), event.getEnd());

            if (startIterator == null) {
                continue;
            }

            while (startIterator.hasNext()) {
                LocalDateTime start = convertToLocalDateTime(startIterator.nextDateTime());
                LocalDateTime end = start.plus(duration);

                if (start.isAfter(dayEnd)) {
                    break;
                } else {
                    updateImportanceDuration(importanceDurationMap, importance, dayStart, dayEnd, start, end);
                }
            }
        }

        return calcBusyness(importanceDurationMap);
    }

    private float calcBusyness(Map<Importance, Duration> importanceDurationMap) {
        float sum = 0;
        for (Map.Entry<Importance, Duration> importanceDuration : importanceDurationMap.entrySet()) {
            Duration duration = importanceDuration.getValue();
            switch (importanceDuration.getKey()) {
                case LOW:
                    sum += (0.5 * duration.toMinutes()) / 60f;
                    break;
                case MEDIUM:
                    sum += duration.toMinutes() / 60f;
                    break;
                case HIGH:
                    sum += (2 * duration.toMinutes()) / 60f;
                    break;
            }
        }

        float f = (1 - sum / 24);

        return f > 0 ? f : 0;
    }

    private void updateImportanceDuration(Map<Importance, Duration> importanceDurationMap,
                                          Importance importance,
                                          LocalDateTime dayStart,
                                          LocalDateTime dayEnd,
                                          LocalDateTime start,
                                          LocalDateTime end) {
        importanceDurationMap.putIfAbsent(importance, Duration.ZERO);
        if (start.isAfter(dayStart) && end.isBefore(dayEnd)) {
            importanceDurationMap.computeIfPresent(importance, (k, v) -> v.plus(Duration.between(start, end)));
        } else if (start.isAfter(dayStart) && end.isAfter(dayEnd)) {
            importanceDurationMap.computeIfPresent(importance, (k, v) -> v.plus(Duration.between(start, dayEnd)));
        } else if (start.isBefore(dayStart) && end.isAfter(dayStart)) {
            importanceDurationMap.computeIfPresent(importance, (k, v) -> v.plus(Duration.between(dayStart, end)));
        }
    }

    private List<TransferredEvent> transferEvents(List<Event> events, ZoneId targetZone) {
        return events.stream()
                .map(e -> transferEvent(e, targetZone))
                .collect(toList());
    }

    private TransferredEvent transferEvent(Event event, ZoneId targetZone) {
        EventDateTime start = event.getStart();
        ZonedDateTime zonedStart = start.getDateTime().atZone(start.getTimeZone()).withZoneSameInstant(targetZone);

        EventDateTime end = event.getEnd();
        ZonedDateTime zonedEnd = end.getDateTime().atZone(end.getTimeZone()).withZoneSameInstant(targetZone);

        TransferredEvent transferredEvent = new TransferredEvent();

        transferredEvent.setStart(zonedStart.toLocalDateTime());
        transferredEvent.setEnd(zonedEnd.toLocalDateTime());
        transferredEvent.setRecurrenceRule(event.getRecurrenceRule());
        transferredEvent.setImportance(event.getImportance());

        return transferredEvent;
    }

    @Transactional
    public void leaveMeeting(User user, UUID id) {
        Meeting meeting = meetingRepository.findById(id).orElseThrow(MeetingNotFoundException::new);

        meeting.getParticipants().removeIf(u -> u.equals(user));
        user.getMeetings().removeIf(m -> m.getId().equals(id));
        user.getCreatedMeetings().removeIf(m -> m.getId().equals(id));
    }

    @Transactional
    public void joinMeeting(User user, String inviteKey) {
        Meeting meeting = meetingRepository.findByInviteKey(inviteKey).orElseThrow(KeyNotFoundException::new);

        meeting.getParticipants().add(user);
        user.getMeetings().add(meeting);
    }

}
