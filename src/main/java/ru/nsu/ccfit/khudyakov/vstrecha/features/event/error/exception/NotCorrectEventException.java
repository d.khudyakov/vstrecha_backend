package ru.nsu.ccfit.khudyakov.vstrecha.features.event.error.exception;

public class NotCorrectEventException extends RuntimeException {

    public NotCorrectEventException(String text) {
        super(text);
    }

}
