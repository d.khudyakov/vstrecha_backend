package ru.nsu.ccfit.khudyakov.vstrecha.error.exception;

public class BadParamsException extends RuntimeException {

    public BadParamsException(String message) {
        super(message);
    }
}
