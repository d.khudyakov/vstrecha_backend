package ru.nsu.ccfit.khudyakov.vstrecha.features.event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Event;
import ru.nsu.ccfit.khudyakov.vstrecha.features.user.entity.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface EventRepository extends JpaRepository<Event, UUID> {

    List<Event> findByUserAndStartDateTimeBetweenAndRecurrenceRuleIsNull(User user, LocalDateTime start, LocalDateTime end);

    List<Event> findByUserAndRecurrenceRuleNotNull(User user);

}
