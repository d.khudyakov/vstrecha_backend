package ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Importance;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
@ApiModel
public class NewEventDto {

    @ApiModelProperty(value = "Event's title")
    @Size(max = 40, message = "Size of title cannot be more than 200 characters. Optional")
    protected String title;

    @ApiModelProperty(value = "Event's start date and time with timezone", position = 1)
    @NotNull(message = "Start date time cannot be empty")
    protected EventDateTimeDto start;

    @ApiModelProperty(value = "Event's end date and time with timezone", position = 2)
    @NotNull(message = "End date time cannot be empty")
    protected EventDateTimeDto end;

    @ApiModelProperty(value = "Event's description")
    @Size(max = 200, message = "Size of description cannot be more than 200 characters. Optional")
    private String description;

    @ApiModelProperty(value = "Event's importance. Possible values: LOW, MEDIUM, HIGH", position = 1)
    @NotNull(message = "Importance cannot be empty. Possible values: LOW, MEDIUM, HIGH")
    private Importance importance;

    @ApiModelProperty(value = "Event's recurrence rule (RFC5545 RRULE). Optional", position = 2)
    private String recurrenceRule;
}
