package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto.deserializer.CustomJsonZonIdDeserializer;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.ZoneId;

@Data
@ApiModel
public class NewMeetingDto {

    @ApiModelProperty(value = "Title")
    @Size(max = 40, message = "Size of description cannot be more than 40 characters")
    private String title;

    @ApiModelProperty(value = "Start date of the selection period")
    @NotNull(message = "Start date time cannot be empty")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate start;

    @ApiModelProperty(value = "End date of the selection period")
    @NotNull(message = "End date time cannot be empty")
    @JsonFormat(pattern = "yyyy-MM-dd")
    protected LocalDate end;

    @ApiModelProperty(value = "Time zone in tz database format of the meeting")
    @NotNull(message = "Time zone cannot be empty")
    @JsonDeserialize(using = CustomJsonZonIdDeserializer.class)
    private ZoneId timeZone;

}
