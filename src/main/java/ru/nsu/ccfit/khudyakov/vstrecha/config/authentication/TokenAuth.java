package ru.nsu.ccfit.khudyakov.vstrecha.config.authentication;

import lombok.Data;

@Data
public class TokenAuth {
    String token;

    public TokenAuth(String token) {
        this.token = token;
    }
}
