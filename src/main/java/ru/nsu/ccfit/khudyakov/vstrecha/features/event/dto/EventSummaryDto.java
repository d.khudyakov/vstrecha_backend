package ru.nsu.ccfit.khudyakov.vstrecha.features.event.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.UUID;

@Data
@ApiModel
public class EventSummaryDto {

    @ApiModelProperty("Event id")
    private UUID id;

    @ApiModelProperty(value = "Event's title")
    protected String title;

    @ApiModelProperty(value = "Event's start date and time with timezone", position = 1)
    protected EventDateTimeDto start;

    @ApiModelProperty(value = "Event's end date and time with timezone", position = 2)
    protected EventDateTimeDto end;

}
