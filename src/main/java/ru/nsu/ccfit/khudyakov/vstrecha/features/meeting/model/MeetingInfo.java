package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Data
public class MeetingInfo {

    private UUID id;

    private String title;

    private LocalDate start;

    protected LocalDate end;

    private String inviteKey;

    private List<Day> days;

    private List<Participant> participants;

}
