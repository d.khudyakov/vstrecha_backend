package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.nsu.ccfit.khudyakov.vstrecha.error.ApiError;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.error.exceptions.KeyNotFoundException;
import ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.error.exceptions.MeetingNotFoundException;

import java.time.LocalDateTime;
import java.util.Collections;

public class MeetingErrorHandler {

    private final static int NOT_FOUND_CODE = 4001;
    private final static int KEY_NOT_FOUND_CODE = 4002;

    @ExceptionHandler(value = {MeetingNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError handleMeetingNotFoundException(MeetingNotFoundException ex) {
        return new ApiError(
                NOT_FOUND_CODE,
                ex.getMessage(),
                Collections.singleton("Please check id of your meeting"),
                LocalDateTime.now());
    }

    @ExceptionHandler(value = {KeyNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError handleKeyNotFoundException(KeyNotFoundException ex) {
        return new ApiError(
                KEY_NOT_FOUND_CODE,
                ex.getMessage(),
                Collections.singleton("Please check key of your meeting"),
                LocalDateTime.now());
    }

}
