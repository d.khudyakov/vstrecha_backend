package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.model;

public enum Busyness {
    HIGH,
    MEDIUM,
    LOW
}
