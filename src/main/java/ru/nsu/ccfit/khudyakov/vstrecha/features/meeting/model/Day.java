package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel
public class Day {

    @ApiModelProperty(value = "Busyness of day")
    private Busyness busyness;

}
