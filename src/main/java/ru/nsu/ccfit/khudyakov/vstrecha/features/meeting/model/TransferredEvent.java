package ru.nsu.ccfit.khudyakov.vstrecha.features.meeting.model;

import lombok.Data;
import ru.nsu.ccfit.khudyakov.vstrecha.features.event.entity.Importance;

import java.time.LocalDateTime;

@Data
public class TransferredEvent {

    private LocalDateTime start;

    private LocalDateTime end;

    private Importance importance;

    private String recurrenceRule;

}
