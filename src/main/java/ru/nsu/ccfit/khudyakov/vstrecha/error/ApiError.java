package ru.nsu.ccfit.khudyakov.vstrecha.error;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@ApiModel
@EqualsAndHashCode(exclude = {"time"})
public class ApiError {
    @ApiModelProperty(value = "error code")
    private Integer errorCode;

    @ApiModelProperty(value = "main info about error")
    private String message;

    @ApiModelProperty(value = "details about error")
    private Set<String> details;

    @ApiModelProperty(value = "time of creation")
    private LocalDateTime time;

    public ApiError(Integer errorCode, String message, Set<String> details, LocalDateTime time) {
        this.errorCode = errorCode;
        this.message = message;
        this.details = details;
        this.time = time;
    }


}
